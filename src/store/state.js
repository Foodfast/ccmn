export default {
  locateApiClient: undefined,
  locateApiCredentials: {
    username: '',
    password: '',
    url: ''
  },
  presenceApiClient: undefined,
  presenceApiCredentials: {
    username: '',
    password: '',
    url: ''
  },
  mapElements: {
    entities: {},
    result: []
  },
  mapImages: {},
  allSites: [],
  previousConectedClients: {
    entities: {},
    result: []
  },
  currentConnectedClients: {
    entities: {},
    result: []
  },
  recentClientsMacs: undefined,
  disconnectedClientsMacs: undefined,
  errors: [],
  showSidebar: false,
  activeClientsUpdaterId: undefined
}
