import Vue from 'vue'
import axios from 'axios'
import * as mutationTypes from './mutation-types'

export default {
  [mutationTypes.SET_API_CREDENTIALS] (state, credentials) {
    state.locateApiCredentials = credentials.locate
    state.presenceApiCredentials = credentials.presence
  },
  [mutationTypes.UNSET_API_CREDENTIALS] (state) {
    state.locateApiClient = undefined
    state.presenceApiClient = undefined
    state.locateApiCredentials = {}
    state.presenceApiCredentials = {}
  },
  [mutationTypes.CREATE_API_CLIENTS] (state) {
    state.locateApiClient = axios.create({
      baseURL: state.locateApiCredentials.url,
      auth: {
        username: state.locateApiCredentials.username,
        password: state.locateApiCredentials.password
      }
    })
    state.presenceApiClient = axios.create({
      baseURL: state.presenceApiCredentials.url,
      auth: {
        username: state.presenceApiCredentials.username,
        password: state.presenceApiCredentials.password
      }
    })
  },
  [mutationTypes.SET_ALL_MAP_ELEMENTS] (state, mapElements) {
    state.mapElements = mapElements
  },
  [mutationTypes.SET_MAP_IMAGE] (state, { name, blob }) {
    let image = state.mapImages[name]
    if (image) URL.revokeObjectURL(image)
    Vue.set(state.mapImages, name, URL.createObjectURL(blob))
  },
  [mutationTypes.SET_RECENT_CLIENT_MACS] (state, macs) {
    state.recentClientsMacs = macs
  },
  [mutationTypes.SET_DISCONNECTED_CLIENT_MACS] (state, macs) {
    state.disconnectedClientsMacs = macs
  },
  [mutationTypes.SET_PREVIOUS_CLIENTS] (state, clients) {
    state.previousConectedClients = clients
  },
  [mutationTypes.SET_ACTIVE_CLIENTS] (state, clients) {
    state.currentConnectedClients = clients
  },
  [mutationTypes.SET_ERROR] (state, error) {
    state.errors.push(error)
  },
  [mutationTypes.REMOVE_LAST_ERROR] (state) {
    state.errors.pop()
  },
  [mutationTypes.TOGGLE_SIDEBAR] (state, showSidebar) {
    state.showSidebar = showSidebar
  },
  [mutationTypes.SET_ACTIVE_CLIENTS_UPDATER_ID] (state, id) {
    state.activeClientsUpdaterId = id
  },
  [mutationTypes.SET_ALL_SITES] (state, sites) {
    state.allSites = sites
  }
}
