import normalizeMapApi from '@/api/map-schema'
import normalizeActiveClients from '@/api/active-clients-schema'
import * as actionTypes from './action-types'
import * as mutationTypes from './mutation-types'

const clientsMacDiff = (master, slave) => {
  return master.filter(mac => {
    return !slave.includes(mac)
  })
}
export default {
  [actionTypes.GET_ALL_MAP_ELEMENTS] ({ commit, state }) {
    state
      .locateApiClient
      .get('/api/config/v1/maps')
      .then(response => {
        commit(
          mutationTypes.SET_ALL_MAP_ELEMENTS,
          normalizeMapApi(response.data)
        )
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_ALL_SITES] ({ commit, state }) {
    state
      .presenceApiClient
      .get('/api/config/v1/sites')
      .then(response => {
        commit(
          mutationTypes.SET_ALL_SITES,
          response.data
        )
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_SITE_SUMMARY] ({ commit, state }, payload) {
    return state
      .presenceApiClient
      .get('/api/presence/v1/kpisummary', {
        params: payload
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_SITE_CONNECTED_COUNT_DAILY] ({ commit, state }, payload) {
    return state
      .presenceApiClient
      .get('/api/presence/v1/connected/daily', {
        params: payload
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_SITE_VISITED_COUNT_DAILY] ({ commit, state }, payload) {
    return state
      .presenceApiClient
      .get('/api/presence/v1/visitor/daily', {
        params: payload
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_SITE_PASSERBY_COUNT_DAILY] ({ commit, state }, payload) {
    return state
      .presenceApiClient
      .get('/api/presence/v1/passerby/daily', {
        params: payload
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_SITE_DWELL_TIME_DAILY] ({ commit, state }, payload) {
    return state
      .presenceApiClient
      .get('/api/presence/v1/dwell/daily', {
        params: payload
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_IMAGE] ({ commit, state }, { name }) {
    state
      .locateApiClient
      .get(
        '/api/config/v1/maps/imagesource/' + name,
        { responseType: 'blob' }
      )
      .then(response => {
        commit(mutationTypes.SET_MAP_IMAGE, { name, blob: response.data })
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.GET_ACTIVE_CLIENTS] ({ commit, state, getters }) {
    state
      .locateApiClient
      .get('/api/location/v2/clients/')
      .then(response => {
        commit(
          mutationTypes.SET_PREVIOUS_CLIENTS,
          state.currentConnectedClients
        )
        commit(
          mutationTypes.SET_ACTIVE_CLIENTS,
          normalizeActiveClients(response.data)
        )

        let disconnectedClients = clientsMacDiff(
          state.previousConectedClients.result,
          state.currentConnectedClients.result
        )
        let recentClients = clientsMacDiff(
          state.currentConnectedClients.result,
          state.previousConectedClients.result
        )
        commit(
          mutationTypes.SET_RECENT_CLIENT_MACS,
          recentClients
        )
        commit(
          mutationTypes.SET_DISCONNECTED_CLIENT_MACS,
          disconnectedClients
        )
      })
      .catch(error => {
        commit(mutationTypes.SET_ERROR, error.response.statusText)
      })
  },
  [actionTypes.START_ACTIVE_CLIENTS_UPDATER] ({ commit, dispatch }) {
    let id = setInterval(() => {
      dispatch(actionTypes.GET_ACTIVE_CLIENTS)
    }, 1500)
    commit(mutationTypes.SET_ACTIVE_CLIENTS_UPDATER_ID, id)
  },
  [actionTypes.STOP_ACTIVE_CLIENTS_UPDATER] ({ commit, state, dispatch }) {
    clearInterval(state.activeClientsUpdaterId)
    commit(mutationTypes.SET_ACTIVE_CLIENTS_UPDATER_ID, undefined)
  },
  [actionTypes.REMOVE_ERROR] (context) {
    if (context.getters.errosCount) {
      context.commit(mutationTypes.REMOVE_LAST_ERROR)
    }
  }
}
