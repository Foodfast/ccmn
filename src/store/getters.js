import filter from 'lodash/filter'

export default {
  isApiClientsCreated (state) {
    return state.locateApiClient && state.presenceApiClient
  },
  locateCredentials: state => state.locateApiCredentials,
  presenceCredentials: state => state.presenceApiCredentials,
  campuses: state => state.mapElements.entities.campuses,
  buildings: state => state.mapElements.entities.buildings,
  floors: state => state.mapElements.entities.floors,
  campusById: (state) => (id) => {
    return state.mapElements.entities.campuses[id]
  },
  campusBuildings: (state) => (campus) => {
    return campus
      .buildingList
      .map(id => state.mapElements.entities.buildings[id])
  },
  buildingById: (state) => (id) => {
    return state.mapElements.entities.buildings[id]
  },
  buildingFloors: (state) => (building) => {
    return building
      .floorList
      .map(id => state.mapElements.entities.floors[id])
  },
  floorById: (state) => (id) => {
    return state.mapElements.entities.floors[id]
  },
  floorZones: (state) => (id) => {
    let floor = state.mapElements.entities.floors[id]
    return floor.zones.map(zone => state.mapElements.entities.zones[zone])
  },
  floorObstacles: (state) => (id) => {
    let floor = state.mapElements.entities.floors[id]
    return floor.obstacles.map(
      obstacle => state.mapElements.entities.obstacles[obstacle]
    )
  },
  floorClients: (state) => (id) => {
    return filter(state.currentConnectedClients.entities.clients,
      (client) => { return client.mapInfo.floorRefId === id }
    )
  },
  imageByName: (state) => (name) => {
    return state.mapImages[name]
  },
  showSidebar: state => state.showSidebar,
  errosCount: state => state.errors.length,
  lastError (state, getters) {
    if (!getters.errosCount) return ''
    return state.errors[getters.errosCount - 1]
  },
  currentConnectedClientsCount: state => {
    return state.currentConnectedClients.result.length
  },
  currentConnectedClients: state => {
    return state.currentConnectedClients.entities.clients
  },
  currentConnectedClientsArray: state => {
    return state.currentConnectedClients.result
  },
  recentClientsMacs: state => state.recentClientsMacs,
  disconnectedClientsMacs: state => state.disconnectedClientsMacs,
  recentClientByMac: (state) => (mac) => {
    return state.currentConnectedClients.entities.clients[mac]
  },
  disconectedClientByMac: (state) => (mac) => {
    return state.previousConectedClients.entities.clients[mac]
  },
  allSites: state => state.allSites
}
