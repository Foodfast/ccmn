import { normalize, schema } from 'normalizr'

const client = new schema.Entity('clients', {}, {
  idAttribute: 'macAddress'
})

const root = [client]

export default function (response) {
  return normalize(response, root)
}
