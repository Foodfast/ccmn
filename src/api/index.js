import axios from 'axios'

const unitCmxLocate = axios.create({
  baseURL: 'https://cisco-cmx.unit.ua',
  auth: {
    username: 'RO',
    password: 'just4reading'
  }
})

const unitCmxPresence = axios.create({
  baseURL: 'https://cisco-presence.unit.ua',
  auth: {
    username: 'RO',
    password: 'Passw0rd'
  }
})

export { unitCmxLocate, unitCmxPresence }
