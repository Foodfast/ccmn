import { normalize, schema } from 'normalizr'

const apInterface = new schema.Entity('apInterfaces', {}, {
  idAttribute (apInterface, ap) {
    return `${ap.radioMacAddress}_${apInterface.slotNumber}`
  }
})

const accessPoint = new schema.Entity('accessPoints', {
  apInterfaces: [apInterface]
}, {
  idAttribute: 'radioMacAddress'
})

const gpsMarker = new schema.Entity('gpsMarkers', {}, {
  idAttribute (marker) {
    return `${marker.geoCoordinate.latitude}_${marker.geoCoordinate.longitude}`
  }
})

const zone = new schema.Entity('zones', {}, {
  idAttribute (zone, floor) {
    return `${floor.aesUidString}_${zone.name}`
  }
})

const obstacle = new schema.Entity('obstacles', {}, {
  idAttribute (obstacle, floor) {
    if (obstacle.obstacleCoordinates.length) {
      let x = obstacle.obstacleCoordinates[0].x
      let y = obstacle.obstacleCoordinates[0].y
      return `${floor.aesUidString}_${x}_${y}`
    }
  }
})

const floor = new schema.Entity('floors', {
  obstacles: [obstacle],
  gpsMarkers: [gpsMarker],
  zones: [zone],
  accessPoints: [accessPoint]
}, {
  idAttribute: 'aesUidString'
})

const building = new schema.Entity('buildings', {
  floorList: [floor]
}, {
  idAttribute: 'aesUidString'
})

const campus = new schema.Entity('campuses', {
  buildingList: [building]
}, {
  idAttribute: 'aesUidString'
})

const root = { campuses: [campus] }

export default function (response) {
  return normalize(response, root)
}
