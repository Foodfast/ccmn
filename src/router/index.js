import Vue from 'vue'
import store from '@/store'
import Router from 'vue-router'
import Index from '@/components/Index'
import ApiSetup from '@/components/ApiSetup'
import Campus from '@/components/Campus'
import CampusList from '@/components/CampusList'
import Building from '@/components/Building'
import BuildingList from '@/components/BuildingList'
import Floor from '@/components/Floor'
import FloorList from '@/components/FloorList'
import KpiSummary from '@/components/KpiSummary'
import Visitors from '@/components/Visitors'
import Dwell from '@/components/Dwell'
import Analitics from '@/components/Analitics'

Vue.use(Router)

const ifApiClientsCreated = (to, from, next) => {
  if (store.getters.isApiClientsCreated) {
    next()
    return
  }
  next({ name: 'apiSetup' })
}

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index,
      beforeEnter: ifApiClientsCreated
    },
    {
      path: '/api-setup',
      name: 'apiSetup',
      component: ApiSetup
    },
    {
      path: '/campus',
      name: 'campus',
      component: CampusList,
      beforeEnter: ifApiClientsCreated,
      children: [
        {
          path: ':campusId',
          name: 'campusById',
          component: Campus,
          children: [
            {
              path: 'buildig',
              name: 'building',
              component: BuildingList,
              children: [
                {
                  path: ':buildingId',
                  name: 'buildingById',
                  component: Building,
                  children: [
                    {
                      path: 'floor',
                      name: 'floor',
                      component: FloorList,
                      children: [
                        {
                          path: ':floorId',
                          name: 'floorById',
                          component: Floor,
                          props: true
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: '/kpi-summary',
      name: 'kpiSummary',
      component: KpiSummary,
      beforeEnter: ifApiClientsCreated
    },
    {
      path: '/visitors',
      name: 'visitors',
      component: Visitors,
      beforeEnter: ifApiClientsCreated
    },
    {
      path: '/dwell-time',
      name: 'dwell',
      component: Dwell,
      beforeEnter: ifApiClientsCreated
    },
    {
      path: '/analitics',
      name: 'analitics',
      component: Analitics,
      beforeEnter: ifApiClientsCreated
    }
  ]
})

export default router
