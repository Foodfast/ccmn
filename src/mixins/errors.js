import { mapGetters } from 'vuex'
import * as actionTypes from '@/store/action-types'

export default {
  computed: {
    ...mapGetters([
      'lastError'
    ])
  },
  watch: {
    lastError (error) {
      console.log(error)
      let vm = this
      this.$toasted.show(
        error,
        {
          position: 'top-right',
          type: 'error',
          theme: 'bubble',
          singleton: true,
          action: {
            text: 'Close',
            onClick (e, toast) {
              vm.$store.dispatch(actionTypes.REMOVE_ERROR)
              toast.goAway(0)
            }
          }
        }
      )
    }
  }
}
